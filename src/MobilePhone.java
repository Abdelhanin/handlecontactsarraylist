import java.util.ArrayList;

public class MobilePhone {
    private ArrayList<Contact> contacts;
    private String myNumber;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.contacts = new ArrayList<Contact>();
    }

    public boolean addNewContact(Contact contact) {
        // Looking if already exists
        if (findContact(contact.getName()) >= 0) {
            System.out.println("Contact is already on file");
            return false;
        }
        // Add new contact
        contacts.add(contact);
        return true;
    }

    public boolean updateContact(Contact oldContact, Contact newContact) {
        int foundPosition = findContact(oldContact);
        // Checking if its founded
        if (foundPosition < 0) {
            System.out.println(oldContact.getName() +", was not found.");
            return false;
        }
        // Update the contact
        this.contacts.set(foundPosition, newContact);
        System.out.println(oldContact.getName() + ", was replaced with " + newContact.getName());
        return true;
    }

    public boolean removeContact(Contact contact) {
        int foundPosition = findContact(contact);
        if (foundPosition < 0) {
            System.out.println(contact.getName() +", was not found.");
            return false;
        }
        this.contacts.remove(foundPosition);
        System.out.println(contact.getName() + ", was deleted.");
        return true;
    }

    public String queryContact(Contact contact) {
        if(findContact(contact) >=0) {
            return contact.getName();
        }
        return null;
    }

    public Contact queryContact(String name) {
        int position = findContact(name);
        if(position >=0) {
            return this.contacts.get(position);
        }

        return null;
    }

    private int findContact(Contact contact) {
        return this.contacts.indexOf(contact);
    }

    private int findContact(String contactName) {
        // Searching by Name
        for (int i = 0; i < this.contacts.size(); i++) {
            Contact contact = this.contacts.get(i);
            if (contact.getName().equals(contactName)) {
                return i;
            }
        }
        return -1;
    }

    public void printContacts() {
        System.out.println("Contact List");
        if (this.contacts.size() == 0) {
            System.out.println("No contact yet in your phone contact, please click on 2 for registering the first one");
        } else {
            for(int i=0; i<this.contacts.size(); i++) {
                System.out.println((i+1) + "." +
                        this.contacts.get(i).getName() + " -> " +
                        this.contacts.get(i).getPhoneNumber());
            }
        }
    }

    private boolean alreadyExist() {
        return false;
    }
}
